#include "csapp.h"
#include <netinet/in.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#define MAXLINE_SERVER      1024

int main(int argc, char* argv[])
{
	int listenfd;
	struct sockaddr_in clientaddr;
	char *port;
    char filename[MAXLINE_SERVER];
    char buffer[MAXLINE_SERVER];
    char *directorio;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);


	while(1)
    {
        struct sockaddr_storage serverStorage;
        socklen_t    addr_size   = sizeof serverStorage;
        int newSocket = accept(listenfd, (struct sockaddr*)&serverStorage, &addr_size);

        //se lee el nombre del archivo enviado por el cliente y se lo almacena en filename
        int  get = read(newSocket, filename, MAXLINE_SERVER - 1);

        filename[get] = '\0';
        fprintf(stdout, "%s\n", filename);

        //se crea la estructura stat con el archivo
        struct stat sbuf;

        directorio = "./servidor/";

        char *ruta;

        ruta = malloc(strlen(directorio) + strlen(filename) + 1);

        strcpy(ruta, directorio);
        strcat(ruta, filename);

        if (stat(ruta, &sbuf) < 0) {

        	write(newSocket, "-1", 2);
        }

        else{

            //si si existe el archivo se envia el tamaño del archivo al cliente
            sprintf(buffer, "%d" ,sbuf.st_size);
            write(newSocket, buffer, 3);

            //se lee el contenido del .txt y se lo almacena en line 
            if(sbuf.st_size >0){

                

                FILE *file = fopen(ruta, "r");
                unsigned char *line = NULL;
                ssize_t read;
                size_t len = 0;

                while ((read = getline(&line, &len, file)) != -1) {
                    //printf("Texto: %s", line);
                }
            
                fprintf(stdout, "Texto: %s\n", line);

                //Se envia el contenido del .txt al cliente

                sleep(2);

                write(newSocket, line, sbuf.st_size + 1);
            }


        }



        close(newSocket);
    }
    close(listenfd);

}